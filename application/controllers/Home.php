<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index($info='')
	{
		if(isset($_POST['user'])&&isset($_POST['pswd']))
		{
			$user = $_POST['user'];
			$pswd = $_POST['pswd'];
			$query = $this->db->get_where('user', array('username' => $user, 'password' => md5($pswd)));
			if($query->num_rows() == 1)
			{
				setcookie('user', $user, time()+3600*24, '/V/');
				redirect('/');
			}
			else $info = 1;
		}
		$data = $this->db->get('barang');
		if(empty(get_cookie('user'))) $this->load->view('login', array('info' => $info));
		else $this->load->view('barang', array('data' => $data));
	}
	public function tbhbrg()
	{
		if(isset($_POST['kode']))
		{
			$kode = $_POST['kode'];
			$barcode = $_POST['barcode'];
			$nama = $_POST['nama'];
			$stok = $_POST['stok'];
			$data = array('kode' => $kode, 'barcode' => $barcode, 'barang' => $nama, 'stok' => $stok);
			if($this->db->insert('barang', $data)) redirect('/');
		}
		if(empty(get_cookie('user'))) $this->load->view('login');
		else $this->load->view('tambah_barang');
	}
	public function hpsbrg($id)
	{
		if($this->db->delete('barang', array('id' => $id))) redirect('/');
	}
	public function edtbrg($id)
	{
		if(isset($_POST['kode']))
		{
			$kode = $_POST['kode'];
			$barcode = $_POST['barcode'];
			$nama = $_POST['nama'];
			$stok = $_POST['stok'];
			$data = array('kode' => $kode, 'barcode' => $barcode, 'barang' => $nama, 'stok' => $stok);
			$this->db->where('id', $id);
			if($this->db->update('barang', $data)) redirect('/');
		}
		$data = $this->db->get_where('barang', array('id' => $id));
		if(empty(get_cookie('user'))) $this->load->view('login');
		else $this->load->view('edit_barang', array('data' => $data->result()[0]));
	}
	public function daftar()
	{
		$error = 0;
		if (isset($_POST['user'])&&isset($_POST['pswd'])) {
			$user = $_POST['user'];
			$pswd = $_POST['pswd'];
			$chck = $this->db->get_where('user', array('username' => $user));
			if($chck->num_rows() == 0)
			{
				$data = array('username' => $user, 'password' => md5($pswd));
				if($this->db->insert('user', $data)) redirect('Home/index/2');
			}
			else $error = 1;
		}
		$this->load->view('daftar', array('error' => $error));
	}
	public function logout()
	{
		setcookie('user', '', time()-3600*24, '/V/');
		redirect('/');
	}
}
