<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/font/css/all.css">

    <title>Data Barang</title>
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-md-2"></div>
    		<div class="col-md-8">
    			<div class="menu">
    				<h1 align="center">Data Barang</h1>
    				<br>
    				<table class="table table-striped table-hover">
                        <thead class="table-primary">
    					       <tr>
    						      <th scope="col"><i class="fas fa-stroopwafel"></i> ID</th>
    						      <th scope="col">KODE</th>
    						      <th scope="col">BARCODE</th>
    						      <th scope="col">BARANG</th>
    						      <th scope="col">STOK</th>
    						      <th scope="col">AKSI</th>
    					       </tr>
                        </thead>
                        <tbody>
    					<?php
    					foreach ($data->result() as $row) {
    					echo '<tr>
    						<th scope="row">'.$row->id.'</th>
    						<td>'.$row->kode.'</td>
    						<td>'.$row->barcode.'</td>
    						<td>'.$row->barang.'</td>
    						<td>'.$row->stok.'</td>
    						<td>
    							<button class="btn btn-danger" data-toggle="modal" data-target="#exampleModal'.$row->id.'">HAPUS</button>
    							<a href="'.site_url('Home/edtbrg/'.$row->id).'"><button class="btn btn-primary">EDIT</button></a>
    							<div class="modal fade" id="exampleModal'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  									<div class="modal-dialog" role="document">
   		 								<div class="modal-content">
      										<div class="modal-header">
        										<h5 class="modal-title" id="exampleModalLabel">Yakin Ingin Menghapus?</h5>
        										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          											<span aria-hidden="true">&times;</span>
        										</button>
      										</div>
      										<div class="modal-footer">
        										<button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
        										<a href="'.site_url('Home/hpsbrg/'.$row->id).'"><button type="button" class="btn btn-primary">Iya, Hapus</button></a>
      										</div>
    									</div>
  									</div>
								</div>
    						</td>
    					</tr>';
    					}
    					?>
                        </tbody>
    				</table>
    				<br>
    				<a href="<?= site_url('Home/tbhbrg') ?>"><button class="btn btn-primary btn-block">TAMBAH BARANG</button></a><br>
                    <button class="btn btn-warning btn-block" data-toggle="modal" data-target="#Logout">KELUAR</button>
                    <div class="modal fade" id="Logout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Yakin Ingin Keluar?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                    <a href="<?= site_url('Home/logout') ?>"><button type="button" class="btn btn-primary">Iya, Keluar</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
    			</div>
    		</div>
    		<div class="col-md-2"></div>
    	</div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url() ?>assets/js/jquery.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>