<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/font/css/all.css">

    <title>Login Page</title>
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-md-4"></div>
    		<div class="col-md-4">
    			<div class="menu">
    				<h1 align="center">Login</h1>
    				<br>
                    <?php
                        if($info==1) {
                    ?>
                    <div class="alert alert-danger">Username atau Password Salah!</div>
                    <?php }
                    elseif($info==2) { ?>
                    <div class="alert alert-success">Berhasil Mendaftar! Silahkan Login!</div>
                    <?php } ?>
    				<form method="post">
    					<input type="text" name="user" class="form-control" placeholder="Username" required><br>
    					<input type="password" name="pswd" class="form-control" placeholder="Password" required><br>
    					<input type="submit" value="LOGIN" class="btn btn-secondary btn-block">
    				</form>
                    <br>
                    <a href="<?= site_url('Home/daftar') ?>"><button class="btn btn-block btn-primary"><i class="fas fa-kiss-beam"></i> BUAT AKUN BARU</button></a>
    			</div>
    		</div>
    		<div class="col-md-4"></div>
    	</div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url() ?>assets/js/jquery.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>