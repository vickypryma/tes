<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">


    <title>Edit Barang</title>
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-md-4"></div>
    		<div class="col-md-4">
    			<div class="menu">
    				<h1 align="center">Edit Barang</h1>
                    <br>
                    <form method="post">
                        <input type="text" name="kode" value="<?= $data->kode ?>" class="form-control"><br>
                        <input type="text" name="barcode" value="<?= $data->barcode ?>" class="form-control"><br>
                        <input type="text" name="nama" value="<?= $data->barang ?>" class="form-control"><br>
                        <input type="number" name="stok" value="<?= $data->stok ?>" class="form-control"><br>
                        <input type="submit" value="EDIT" Barang" class="btn btn-primary btn-block">
                    </form>
                    <br>
                    <a href="<?= site_url('/') ?>"><button class="btn btn-block btn-info">KEMBALI</button></a>
    			</div>
    		</div>
    		<div class="col-md-4"></div>
    	</div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url() ?>assets/js/jquery.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>